module Api
  class RacersController < ApplicationController

    rescue_from Mongoid::Errors::DocumentNotFound do |exception|
      render plain: "woops: cannot find race[#{params[:id]}]", status: :not_found
    end

    # GET /racers
    # GET /racers.json
    def index
      if !request.accept || request.accept == "*/*"
        render plain: "/api/racers"
      else
        #real implementation ...
      end
    end

    # GET /racers/1
    # GET /racers/1.json
    def show
      if !request.accept || request.accept == "*/*"
        render plain: "/api/racers/#{params[:id]}"
      else
        #real implementation ...
      end
    end

    def show_entry
      if !request.accept || request.accept == "*/*"
        render plain: "/api/racers/#{params[:racer_id]}/entries/#{params[:id]}"
      else
        #real implementation ...
      end
    end

    def show_entries
      if !request.accept || request.accept == "*/*"
        render plain: "/api/racers/#{params[:racer_id]}/entries"
      else
        #real implementation ...
      end
    end

    # GET /racers/new
    def new
    end

    # GET /racers/1/edit
    def edit
    end


    # POST /racers
    # POST /racers.json
    def create
    end


    # PATCH/PUT /racers/1
    # PATCH/PUT /racers/1.json
    def update
    end

    # DELETE /racers/1
    # DELETE /racers/1.json
    def destroy
    end
  end
end
