module Api
  class RacesController < ApplicationController

    rescue_from Mongoid::Errors::DocumentNotFound do |exception|
      if !request.accept || request.accept == "*/*"
        render plain: "woops: cannot find race[#{params[:id]}]", status: :not_found
      else
        render :status=>:not_found,
               :template=>"api/error_msg",
               :locals=>{ :msg=>"woops: cannot find race[#{params[:id]}]"}
      end
    end

    rescue_from ActionView::MissingTemplate do |exception|
      Rails.logger.debug exception
      render plain: "woops: we do not support that content-type[#{request.accept}]", status: :unsupported_media_type
    end

    # GET /races
    # GET /races.json
    def index
      if !request.accept || request.accept == "*/*"
        render plain: "/api/races, offset=[#{params[:offset]}], limit=[#{params[:limit]}]"
      else
        #real implementation ...
      end
    end

    # GET /races/1
    # GET /races/1.json
    def show
      if !request.accept || request.accept == "*/*"
        render plain: "/api/races/#{params[:id]}"
      else
        @race = Race.find(params[:id])
        #render json: @race, status: :ok
        render @race, status: :ok
      end
    end

    def show_result
      if !request.accept || request.accept == "*/*"
        render plain: "/api/races/#{params[:race_id]}/results/#{params[:id]}"
      else
        #real implementation ...
        @result=Race.find(params[:race_id]).entrants.where(:id=>params[:id]).first
        render :partial=>"result", :object=>@result
      end
    end

    def show_results
      if !request.accept || request.accept == "*/*"
        render plain: "/api/races/#{params[:race_id]}/results"
      else
        #real implementation ...
        @race=Race.find(params[:race_id])
        if stale?(@race)
          @entrants=@race.entrants
          render template: "api/results/index"
        end
      end
    end

    def update_result
      result=params[:result]
      entrant=Race.find(params[:race_id]).entrants.where(:id=>params[:id]).first
      if result
        if result[:swim]
          entrant.swim=entrant.race.race.swim
          entrant.swim_secs = result[:swim].to_f
        end
        if result[:t1]
          entrant.t1=entrant.race.race.t1
          entrant.t1_secs = result[:t1].to_f
        end
        if result[:bike]
          entrant.bike=entrant.race.race.bike
          entrant.bike_secs = result[:bike].to_f
        end
        if result[:t2]
          entrant.t2=entrant.race.race.t2
          entrant.t2_secs = result[:t2].to_f
        end
        if result[:run]
          entrant.run=entrant.race.race.run
          entrant.run_secs = result[:run].to_f
        end
        entrant.save
      end
      if !request.accept || request.accept == "*/*"
        render plain: "/api/races/#{params[:race_id]}/results/#{params[:id]}"
      else
        #real implementation ...
        @result = entrant
        render :partial=>"result", :object=>@result
      end
    end

    # GET /races/new
    def new
    end

    # GET /races/1/edit
    def edit
    end

    # POST /races
    # POST /races.json
    def create
      #render plain: :nothing, status: :ok
      if !request.accept || request.accept == "*/*"
        if params[:race]
          render plain: params[:race][:name], status: :ok
        else
          render plain: :nothing, status: :ok
        end
      else
        race_hash = race_params
        @race = Race.new(race_hash)
        @race.save
        render plain: @race.name, status: :created, content_type: 'text/plain'
      end
    end

    # PATCH/PUT /races/1
    # PATCH/PUT /races/1.json
    def update
      @race = Race.find(params[:id])
      if @race.update(race_params)
        render json: @race, status: :ok
      else
        render json: @race, status: :unprocessable_entity
      end
    end

    # DELETE /races/1
    # DELETE /races/1.json
    def destroy
      @race = Race.find(params[:id])
      @race.destroy
      render :nothing=>true, :status => :no_content
    end

    private

    # Never trust parameters from the scary internet, only allow the white list through.
    def race_params
      params.require(:race).permit(:name, :date)
    end

  end
end
