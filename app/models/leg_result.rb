class LegResult
  include Mongoid::Document

  field :secs, type: Float
  field :bib, type: Integer
  field :o, as: :overall, type: Placing
  field :gender, type: Placing
  field :group , type: Placing

  validates :event, :presence => true

  after_initialize :calc_ave
  embedded_in :entrant
  embeds_one :event, :as => 'parent'


  def calc_ave
    #subclasses will calc event-specific ave
  end

  def secs= value
    self[:secs] = value
    calc_ave
  end


end
